import numpy as np
import clase as C

def Epanechnikov(Xn,Xm,h):
    N = len(Xn)
    sol = np.zeros(N)

    for i in range(N):
        A = abs(Xn-Xm)/h
        if A<=1:
            sol[i] = 3/4*(1-A**2)
        elif A>1:
            sol[i] = 0
    
    return sol


Solucion = C.SuavKernel('Colombia_COVID19_Coronavirus_casos_diarios.csv',h=18/2)
Solucion.SuavizadoGauss()
Solucion.SuavizadoEpanechnikov()
Solucion.SuavizadoTriCube()
Solucion.GuardarFigura("Prueba")