import numpy as np
import matplotlib.pyplot as plt

class Ising:
    def __init__(self,L,N,ti,tf,Nt):
        self.L = L
        self.N = N
        self.Nt = Nt
        self.temperatures = np.linspace(tf,ti,Nt)

    def igrid(self):
        return np.random.choice([-1, 1], size=(self.L,self.L))

    def metropolis(self, grid, beta):
        i, j = np.random.randint(0, self.L, 2)
        spin = grid[i, j]
        neighbors= grid[(i+1)%self.L,j] + grid[(i-1)%self.L,j] + grid[i, (j+1)%self.L] + grid[i,(j-1)%self.L]
        delta_energy = 2 * spin * neighbors
        if delta_energy < 0 or np.random.rand() < np.exp(-beta * delta_energy):
            grid[i, j] = -spin

    def calc_magnetic(self,grid):
        return np.sum(grid)/self.L**2

    def calc_energia(self,grid):
        neighbors= np.roll(grid, 1, axis=0) + np.roll(grid, -1, axis=0) + np.roll(grid, 1, axis=1) + np.roll(grid, -1, axis=1)
        return -np.sum(grid * neighbors)

    def sheat(self,energies,beta):
        energys = energies**2
        menergy = np.mean(energies)
        menergys = np.mean(energys)
        return (menergys-menergy**2)/((beta**2)*self.L**4)

    def simulate(self,beta):
        grid = self.igrid()
        magnetizations = []
        energies = []

        for _ in range(self.N):
            self.metropolis(grid,beta)
            magnetization = self.calc_magnetic(grid)
            energy = self.calc_energia(grid)

            magnetizations.append(np.abs(np.mean(magnetization)))
            energies.append(energy)

        return np.array(magnetizations), np.array(energies)

    def magnetic(self):
        magnetizationsaverage = []

        for temp in self.temperatures:
            beta = 1/temp
            magnetization, _ = self.simulate(beta)
            magnetizationsaverage.append(np.mean(magnetization))

        plt.plot(self.temperatures,magnetizationsaverage,color="r",marker="o",linestyle="-",markersize=2)
        plt.title("Magnetización")
        plt.xlabel("Temperatura (ua)")
        plt.ylabel("Magnetización (ua)")
        plt.grid()
        plt.savefig("magnetizacion.png")

    def calor(self):
        sheats = []
        for temp in self.temperatures:
            beta = 1 / temp
            _, energy = self.simulate(beta)
            sheats.append(self.sheat(energy, beta))

        plt.plot(self.temperatures,sheats,color="r",marker="o",linestyle="-",markersize=5)
        plt.title("Calor específico")
        plt.xlabel("Temperatura (ua)")
        plt.ylabel("Calor Específico (ua)")
        plt.grid()
        plt.savefig("calor.png")