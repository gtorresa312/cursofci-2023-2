from ising import isingModel as isi

if __name__ == "__main__":
    print("empezó")

    L = [2,4,6] #cuadricula de espines de lado L
    nt = 50 # número de puntos en la gráfica
    paso = 1500 #Para la simulación de montecarlo se intera 1000 veces
    a = 1.5 #valor inicial de temperatura
    b  = 3.5 #valor final de temperatura
    
    for i in L:
        model = isi(i,nt,paso,a,b)
        model.plot() #grafica 