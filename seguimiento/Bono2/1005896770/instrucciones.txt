Integracion con Monte Carlo:

Para hacer una integracion definida con Monte Carlo, instale
la librería con:

pip install MCIntAna

Defina la función que quiere integrar. Ejemplo:

def f(x):
  return x ** 2 * np.cos(x)

Ahora damos un ejemplo al usar la librería:

import numpy as np
from MCIntAna.modulo import mc_integ

def f(x):
  return x ** 2 * np.cos(x)

#podemos usar, por ejemplo:
x_inicial = 0
x_final = np.pi
numPasos = 1000

integ = mc_integ(f, x_inicial, x_final, numPasos)

print(integ.MC()[0])

Esto imprime el resultado de la integral definida.



Suavizado con kernel gaussiano:

Para instalar la librería, use:

pip install kernAna

Hay que definir el path al documento con los datos como un 
string. Por ejemplo:

nombre = 'Colombia_COVID19_Coronavirus_casos_diarios.csv'

Ahora un ejemplo utilizandolo:

from kernAna.modulo import suavizado

nombre = 'Colombia_COVID19_Coronavirus_casos_diarios.csv'
sigma = 50
y = suavizado(nombre, sigma)
y.figPlot()

Esto devolverá la gráfica suavizada del numero de casos 
diarios del Coronavirus. Mientras mayor sea el sigma,
más suavizada estará la función.