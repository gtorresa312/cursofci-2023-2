import numpy as np
import matplotlib.pyplot as plt

from pozoEv import pozoTemporal


if __name__=="__main__":
    print("Procesando...\n")
    m=1
    L=1
    estados=(1,2) #Puede poner cualquier estado mayor a 0
    pozo=pozoTemporal(m,L,estados)
    pozo.animplot(fps=20) #Ajustar los fps para mejor visualización

    