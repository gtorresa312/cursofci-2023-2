import numpy as np
import sympy as sp
import matplotlib.pyplot as plt

class MC:
    def __init__(self, a, b, c, n, f):
        """ 
        a: Punto inicial de evaluacion
        b: Punto final de evaluacion 
        c: Puntos a evaluar entre los limites
        n: Numero de puntos a evaluar
        f: Funcion a integrar
        """
        self.a=a
        self.b=b
        self.c=c
        self.n=n
        self.f=f

    def Intervalo(self):
        x=np.linspace(self.a,self.b,self.c)
        return x
    
    def Integrate(self):
        sum = 0
        p = sp.symbols('p')
        f = sp.lambdify(p, self.f, 'numpy')

        for i in range(self.n):
            x = np.random.uniform(self.a,self.b)
            sum += (f(x))
        return ((self.b-self.a)/self.n)*sum
    
    def IntegrateAnalitical(self):
        p = sp.symbols('p')
        integral = sp.integrate(self.f, p)
        df=sp.lambdify(p, integral, 'numpy')
        int = df(self.b)-df(self.a)
        return int

    def Plot(self):
        s=np.arange(0, self.n, int(self.n/100))
        C=[]
        for i in range(len(s)):
            A=self.Integrate(n=i)
            B=self.IntegrateAnalitical(n=i)
            C.append(np.abs(A-B))
        plt.title('Gráfico de convergencia')
        plt.xlabel('Número de iteraciones')
        plt.ylabel('Diferencia entre valor analítico y estimado')
        plt.plot(s,C)
        plt.savefig('Montecarlo/Seguimiento2/Problema1.png')
        
        
        
        