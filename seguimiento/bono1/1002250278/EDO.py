import numpy as np
import matplotlib.pyplot as plt
import sympy as sp
from scipy.integrate import solve_ivp as ivp


class edo:
    def __init__(self,a,b,n,y_0,f,punto=False):
        "a es el pinit"
        self.a = a
        self.b = b 
        self.n = n 
        self.punto = punto
        if self.punto:
            self.b=self.punto
        self.x = []
        self.y = []
        self.y_0 = y_0
        self.f = f
        self.xa = sp.Symbol("x")
        self.ya = sp.Symbol("y")
        self.yr = [] #para el rk4


    def h(self):
        return (self.b-self.a)/self.n
    
    
    def X(self):
        self.x=np.arange(self.a,self.b+self.h(),self.h())
        return self.x
        
    def euler(self):
        self.X()
        self.y=[self.y_0]
        for i in range(self.n):
            self.y.append(self.y[i]+self.h()\
                        *self.f(self.x[i],self.y[i]))
        return self.x, self.y
    
    def solanalitica(self):
        yanalm=ivp(self.f, np.array([self.a, self.punto]), np.array([self.y_0]), t_eval=np.linspace(self.a, self.punto, self.n))
        xanal, yanal = yanalm.t, yanalm.y[0]
        return xanal, yanal
    
    def rk4(self):
        self.X()
        self.yr=[self.y_0]
        for i in range(self.n):
            k1 = self.f(self.x[i],self.yr[i])
            k2 = self.f(self.x[i]+self.h()/2, self.yr[i]+self.h()*k1/2)
            k3 = self.f(self.x[i]+self.h()/2, self.yr[i]+self.h()*k2/2)
            k4 = self.f(self.x[i]+self.h(),   self.yr[i]+self.h()*k3)
            self.yr.append(self.yr[i]+self.h()*(k1+2*k2+2*k3+k4)/6)
        return self.x, self.yr
    
    def figPlotFull(self):
        plt.figure(figsize=(10,5))
        xeuler, yeuler= self.euler()
        xkutta, ykutta= self.rk4()
        xanal, yanal = self.solanalitica()
        plt.plot(xeuler,yeuler,label="sol_euler")
        plt.plot(xkutta,ykutta,label="sol_rk4")
        plt.plot(xanal,yanal,label="sol_analitica")
        plt.legend()
        plt.savefig("solFull.png")