from euler import EDO
#NOTA 5
if __name__ == "__main__":
    
    # parametros
    a = 0       # limite inferior
    b = 1       # limite superior
    n = 100      # numero de pasos 
    y0 = 1      # condicion inicial
    punto = 0.7 # punto a evaluar, debe estar entre a y b.
    
    #Para valores pequeños de n, el metodo de Euler es inestable.
    #por tanto, el "punto" para valores pequeños de "n" no va coincidir con la grafica del mismo metodo
    #por que tanto el "punto" como la grafica continua de Euler fueron calculadas con intervalos diferentes. 
    #Espero solucionar este problema en un futuro.
    
    #colecciones de funciones tales que f(x,y) = y'.
    function = [lambda x,y: 5*y/(1+x)  ,\
                lambda x,y: x*3/(y*2) - 5*x - y*x,\
                lambda x,y: (y**-2)*(y*x -x**2)  
                ]
     
    #Declaracion de la clase EDO
    soledo = EDO(a,b,n,y0,function[0],punto)
    
    #grafica
    soledo.figplot()
    
    #Mas adelante espero agregar try y except para evitar errores de ejecucion.