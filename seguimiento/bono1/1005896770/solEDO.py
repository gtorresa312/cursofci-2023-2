
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

class ecuacion:


    """
    Clase para solucionar ecuaciones diferenciales usando métodos de Euler, Runge-Kutta y analítico.
    """


    def __init__(self, x_ini, y_ini, numPasos, x_buscado = 4):
        self.yo = y_ini
        self.xo = x_ini
        self.N = numPasos
        self.xf = x_buscado

        self.h = (self.xf - self.xo) / self.N


    def f(self, x, y):
        #Función a resolver
        return 5 * y / (x + 1)


    def Euler(self):

        #Arreglos para almacenar los puntos
        self.yEu = np.zeros(self.N)
        self.yEu[0] = self.yo
        self.xEu = np.zeros(self.N)
        self.xEu[0] = self.xo

        #Algoritmo para el método de Euler
        for i in range(self.N-1):
            self.yEu[i+1] = self.yEu[i] + self.h * self.f(i * self.h, self.yEu[i])
            self.xEu[i+1] = self.xEu[i] + self.h

        return self.xEu, self.yEu


    def RK(self):

        #Arreglos para almacenar los puntos
        self.xRK = np.zeros(self.N)
        self.xRK[0] = self.xo
        self.yRK = np.zeros(self.N)
        self.yRK[0] = self.yo

        #Algoritmo de solución del método Runge-Kutta
        for i in range(self.N-1):
            k1 = self.h * self.f(i * self.h, self.yEu[i])
            k2 = self.h * self.f(i * self.h + 0.5 * self.h, self.yEu[i] + 0.5 * k1)
            k3 = self.h * self.f(i * self.h + 0.5 * self.h, self.yEu[i] + 0.5 * k2)
            k4 = self.h * self.f(i * self.h + self.h, self.yEu[i] + k3)

            self.yRK[i+1] = self.yRK[i] + 1/6 * (k1 + 2 * k2 + 2 * k3 + k4)
            self.xRK[i+1] = self.xEu[i] + self.h
        
        return self.xRK, self.yRK
    

    def solucionAnalitica(self):
            
            #Creación de los puntos de solución usando métodos analíticos
            t_eval = np.linspace(self.xo, self.xf, num=self.N)
            self.sol = solve_ivp(self.f, [self.xo, self.xf], [self.yo], t_eval= t_eval)

            return self.sol


    def figPlot(self):

        #Graficación de las tres soluciones dadas en el mismo gráfico
        plt.figure(figsize = (10, 5))
        plt.plot(self.xEu, self.yEu, label = 'sol_euler')
        plt.plot(self.xRK, self.yRK, label = 'sol_RK')
        plt.plot(self.sol.t, self.sol.y[0], label = 'sol_analítica')
        plt.title('Soluciones a f(x) entre {} y {}'.format(self.xo, self.xf))
        plt.legend()
        plt.savefig("solEDO_{}.png".format(self.xf))


        
        